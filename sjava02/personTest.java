package sjava02;

class PersonTest {
    public static void main(String[] args) {

        Person p1 = new Person("Pepe", 44);
        p1.saluda();

        Person p2 = new Person(18, "Marta");
        p2.saluda();

        Person p3 = new Person();
        p3.nom = "Raul";
        p3.setEdat(34);
        p3.saluda();
        
        Person px;
        px = p1;
        px.saluda();
        px = p2;
        px.saluda();
        px = null;
        px.saluda();

    }
}