package sjava02;
class Person {
    String nom;
    private int edat;

    public Person() {

    }

    public Person(String nom, int edat) {
        this.nom = nom;
        this.edat = edat;
    }

    public Person(int edat, String nom) {
        this.nom = nom;
        this.edat = edat;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    void saluda() {
        System.out.println("Hola em dic " + nom + " i tinc " + edat + " anys");
    }
}