import java.util.Scanner;

public class PideNums {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num = 0;
        do {
            System.out.println("Entra un num:");
            try {
                //INTRODUCIMOS EL NUMERO Y SUMAMOS AL TOTAL
                num = keyboard.nextInt();
                total += num;
            } catch (Exception e) {
                //PULSA 0 PARA SALIR
                System.out.println("***Dato incorrecto -0 para salir***");
                keyboard.next();
            }
            //PREGUNTAME MIENTRAS EL NUMERO SEA MAYOR A 0
        } while (num > 0);
        System.out.println("La suma es: " + total);
        keyboard.close();
    }
}
