
public class SumaNums {
    public static void main(String[] args) {
        int total = 0;
        for (String s : args) {
            //TRANSFORMAMOS EL VALOR CONTENIDO EN EL String s de args A UN Integer
            int num = Integer.parseInt(s);
            total = total + num;
        }
        System.out.println("El total es: " + total);
    }
}
